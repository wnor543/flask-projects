#UShorten

## 使用方法

+ 该web程序基于Python 3编写；
+ 安装依赖：`pip install -r requirements.txt`；
+ 配置MongoDB URI：
  + 在`app.py`文件中修改：`pymongo.MongoClient()`，将你的MongoDB URI放进括号中
+ 运行：python app.py
+ 在浏览器中输入：`127.0.0.1:8080`
+ 访问统计信息页面：`127.0.0.1:8080/<slug>/status`

----

## 说明

+ [算法的实现](https://hufangyun.com/2017/short-url/)：
  + 自增序列算法，也叫永不重复算法：设置`id`自增，一个`10进制``id`对应一个`62进制`的数值，1对1，也就不会出现重复的情况。这个利用的就是低进制转化为高进制时，字符数会减少的特性。本应用只使用58个字符，去除掉前端容易混淆的`1`、`l`、`0`、`o`四个字符，并随机排列字符。

+ 自定义短链：长度不能小于四位字符。

+ 提供短链接的浏览次数、原链接等信息页面。

+ 访问密码保护：提供创建短链时添加访问密码，用于限制短链接的访问以及其数据页面的访问。

[邮箱：wnor@live.cn](mailto:wnor@live.cn)


## 截图

1. 界面1：
 ![首页](https://screenshotscdn.firefoxusercontent.com/images/9c3efce3-9c71-487e-ba8d-61e1c2f6941a.png)

2. 界面2：
 ![短链生成界面](https://screenshotscdn.firefoxusercontent.com/images/392582ba-aa3f-47f4-90d0-8ab653b12720.png)

3. 界面3：
 ![短链信息界面](https://screenshotscdn.firefoxusercontent.com/images/ebd50954-c331-4f94-9703-247af6ef1ac1.png)