import os
from functools import wraps
import json

from flask import Flask, jsonify, request, render_template, url_for, redirect
import pymongo

from shorten import encode_url

app = Flask(__name__)

app.config['SECRET_KEY'] = 'Hard string to guess!'

client = pymongo.MongoClient('mongodb://{}:{}@ds141068.mlab.com:41068/todo'.format(os.environ.get('MONGO_URI'), os.environ.get('PD')))
db = client.todo
coll_url = db.short_url
# coll_url.drop()
# index_slug = coll_url.create_index([('slug', pymongo.ASCENDING)], unique=True)
coll_url.save({'url_id': 0})

def get_new_id():
    return coll_url.find_and_modify(update={'$inc': {'url_id': 1}}, new=True).get('url_id')


def check_slug(f):
    @wraps(f)
    def wrapper(slug, *args, **kwargs):
        result = coll_url.find_one({'slug': slug})
        if not result:
            return '<h2>Not Found!</h2>'
        return f(result, slug, *args, **kwargs)
    return wrapper


@app.route('/', methods=['GET'])
def index():
    print(request.host_url)
    return render_template('index.html')

@app.route('/', methods=['POST'])
def shorten_url():
    if request.method == 'POST':
        url = request.form.get('url')
        password = request.form.get('password')
        custom_slug = request.form.get('customslug')
        protect_list = request.form.get('protect').split(',')
        pwvs, pwst = True if 'pwvs' in protect_list else False,\
            True if 'pwst' in protect_list else False
        
        url_id = get_new_id()
        slug = custom_slug if custom_slug else encode_url(url_id)
        if coll_url.find_one({'slug': slug}):
            return jsonify({'Error': u'该短链接已存在！'})
        
        coll_url.insert_one({'url_id': url_id, 'url': url, 'slug': slug,\
                'pwst': pwst, 'pwvs': pwvs, 'password': password, 'visit_count': 0})
        
        return jsonify({'OK': request.host_url + slug}), 201

@app.route('/<slug>', methods=['GET'])
@check_slug
def view_url(result, slug):
    if not result.get('pwvs'):
        coll_url.update_one({'slug': result.get('slug')}, {'$inc': {'visit_count': 1}})
        return redirect(result.get('url'))
    
    return redirect(request.host_url + slug + '/protected')

@app.route('/<slug>/protected', methods=['GET'])
@check_slug
def protected_url(result, slug):
    
    return render_template('protect.html')

@app.route('/<slug>/protected', methods=['POST'])
@check_slug
def check_password(result, slug):
    if request.method == 'POST':
        password = request.form.get('password')
        if password != result.get('password'):
            return jsonify({'Error': u'密码不匹配！'})
        else:
            coll_url.update_one({'slug': result.get('slug')}, {'$inc': {'visit_count': 1}})
            return jsonify({'redirect': result.get('url')})

@app.route('/<slug>/status', methods=['GET'])
@check_slug
def status(result, slug):
    short_url = request.host_url + slug
    url = result.get('url')
    visit_count = result.get('visit_count')
    if not result.get('pwst'):
        return render_template('unprotect.html', visit_count=visit_count, url=url, short_url=short_url)
    else:
        return redirect(request.host_url + slug + '/status/protected')

@app.route('/<slug>/status/protected', methods=['GET'])
@check_slug
def protected_status(result, slug):
    return render_template('protect.html')

@app.route('/<slug>/status/protected', methods=['POST'])
@check_slug
def check_status_password(result, slug):
    if request.method == 'POST':
        password = request.form.get('password')
        if password != result.get('password'):
            return jsonify({'Error': u'密码不匹配'})
        else:
            output = {}
            output['short_url'] = request.host_url + slug
            output['url'] = result.get('url')
            output['visit_count'] = result.get('visit_count')
            return jsonify(['status', output])

if __name__ == '__main__':
    app.run(debug=False, port=8080, host='0.0.0.0')